#ifndef IMAGE_TRANSFORMER_TRANSFORMS_H
#define IMAGE_TRANSFORMER_TRANSFORMS_H
#include "image.h"

struct image* rotate(struct image* image);
#endif //IMAGE_TRANSFORMER_TRANSFORMS_H
