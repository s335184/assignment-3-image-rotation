#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include "stdint.h"
#include <malloc.h>

struct __attribute__((packed)) pixel { uint8_t r, g, b; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

enum read_status
{
    READ_OK = 0,
    READ_INVALID_FILE,
    READ_INVALID_HEADER,
    READ_FILE_NOT_FOUND
};

enum write_status
{
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_HEADER_ERROR,
    WRITE_FILE_NOT_FOUND,
};

struct image* create_image(uint64_t width, uint64_t height);
void free_image(struct image* image);
#endif //IMAGE_TRANSFORMER_IMAGE_H
