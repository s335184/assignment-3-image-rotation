//
// Created by HP on 30.01.2023.
//
#include "image.h"

struct image* create_image(uint64_t width, uint64_t height){
    struct image* new_image = malloc(sizeof(struct image));
    new_image->width = width;
    new_image->height = height;
    new_image->data = malloc(width*height*sizeof(struct pixel));
    return new_image;
}

void free_image(struct image* image){
    free(image->data);
    free(image);
}
