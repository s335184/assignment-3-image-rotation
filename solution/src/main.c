
#include "bmp.h"
#include "transforms.h"

int main( int argc, char** argv ) {
    if(argc != 3){
        printf("Usage of the command: /image-transformer <source-image> <transformed-image>");
        return 1;
    }
    FILE* read_file = fopen(argv[1], "rb");

    FILE* write_file = fopen(argv[2], "wb");

    if(!read_file){
        fprintf(stderr, "Error while opening the file to read! (argument not pointing to an existing file?)");
        return 1;
    }
    if(!write_file){
        fprintf(stderr, "Error while opening the file to write to! (argument not pointing to an existing file?)");
        return 1;
    }

    struct image* image = NULL;

    enum read_status r_status = from_bmp(read_file, &image);
    switch (r_status) {
        case READ_OK:
            break;
        case READ_INVALID_FILE:
            fprintf(stderr, "Unknown file error!");
            return 1;
        case READ_INVALID_HEADER:
            fprintf(stderr, "Header error!");
            return 1;
        case READ_FILE_NOT_FOUND:
            fprintf(stderr, "File not found while trying to read!");
            return 1;
    }

    struct image* rotated = rotate(image);
    free_image(image);

    enum write_status w_status = to_bmp(write_file, rotated);
    free_image(rotated);
    switch (w_status) {
        case WRITE_OK:
            break;
        case WRITE_ERROR:
            fprintf(stderr, "Error while trying to write data into the file!");
            return 1;
        case WRITE_HEADER_ERROR:
            fprintf(stderr, "Error while writing the header into the file!");
            return 1;
        case WRITE_FILE_NOT_FOUND:
            fprintf(stderr, "File not found while trying to write!");
            return 1;
    }

    printf("Rotation successful!");
    return 0;
}
