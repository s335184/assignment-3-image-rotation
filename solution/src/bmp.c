//
// Created by HP on 26.01.2023.
//

#include "bmp.h"

size_t sizeof_image(struct image* const image){
    return (image->width*sizeof(struct pixel) + get_padding(image->width)) * (image->height);
}

size_t sizeof_file(struct image* const image){
    return sizeof_image(image) + sizeof(struct bmp_header);
}

struct bmp_header* generate_header(struct image* const image){
    struct bmp_header* new_header = malloc(sizeof(struct bmp_header));
    *new_header = (struct bmp_header){
            .bfType = 0x4d42,
            .bfileSize = sizeof_file(image),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = sizeof_image(image),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };

    return new_header;
}

uint8_t get_padding(size_t width) {
    size_t bytes = width*sizeof(struct pixel);
    uint8_t padding = (uint8_t)(4 - bytes%4) % 4;
    return padding;
}

bool skip_padding(FILE* const in, uint8_t padding){
    return fseek(in, padding, SEEK_CUR);
}

enum read_status from_bmp(FILE* const in, struct image** const image){
    struct bmp_header header;
    if(!fread(&header, sizeof(struct bmp_header), 1, in)){
        return READ_INVALID_HEADER;
    }

    *image = create_image(header.biWidth, header.biHeight);
    uint8_t padding = get_padding(header.biWidth);
    for(size_t i = 0; i<(*image)->height; i++){
        for(size_t j = 0; j<(*image)->width; j++){
            size_t index = j + i*(*image)->width;
            bool is_valid = fread((*image)->data + index, sizeof(struct pixel), 1, in);
            if(!is_valid){
                free_image(*image);
                return READ_INVALID_FILE;
            }
        }
        if(skip_padding(in, padding)){
            free_image(*image);
            return READ_FILE_NOT_FOUND;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* const out, struct image* const image){
    if(!out){
        return WRITE_FILE_NOT_FOUND;
    }
    struct bmp_header* header = generate_header(image);
    if(!fwrite(header,sizeof(struct bmp_header), 1, out)){
        free(header);
        return WRITE_HEADER_ERROR;
    }
    free(header);


    const size_t padding = get_padding(image->width);
    const uint64_t zero_bytes = 0;
    for(size_t i = 0; i<image->height; i++){
        size_t index = i*(image->width);
        if(fwrite(image->data + index, sizeof(struct pixel), image->width, out) != image->width){
            return WRITE_ERROR;
        }
        if(fwrite(&zero_bytes, 1, padding, out) != padding){
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
