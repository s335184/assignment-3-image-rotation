//
// Created by HP on 08.03.2023.
//

#include "transforms.h"

struct image* rotate(struct image* const image){
    struct image* new_image = create_image(image->height, image->width);
    for(size_t i = 0; i<image->width; i++){
        for(size_t j = 0; j<image->height; j++){
            size_t new_index = j + i*image->height;
            size_t old_index = i + (image->height - j - 1) * image->width;
            new_image->data[new_index] = image->data[old_index];
        }
    }
    return new_image;
}
